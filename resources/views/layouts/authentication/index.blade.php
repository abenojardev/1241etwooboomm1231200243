<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
    <title>@yield('title') | {{ env('APP_NAME') }}</title> 
    <link rel="stylesheet" href="{{ URL::asset('assets/fonts/font.google.css') }}" media="all"> 
    <link href="{{ URL::asset('assets/vendors/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/vendors/themify-icons/themify-icons.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/vendors/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet" /> 
    <link href="{{ URL::asset('assets/css/app.min.css') }}" rel="stylesheet" /> 
    <link href="{{ URL::asset('assets/css/custom.css') }}" rel="stylesheet" /> 
    <style>
        body {
        	background-color: #eff4ff;
        }
        .auth-wrapper {
        		flex: 1 0 auto;
        		display: flex;
        		align-items: center;
        		justify-content: center;
        		padding: 50px 15px 30px 15px;
        }
        .auth-content {
        		max-width: 900px;
        		flex-basis: 900px;
        		box-shadow: 0 1px 15px 1px rgba(62, 57, 107, .07);
        }
        .home-link {
        		position: absolute;
        		left: 5px;
        		top: 10px;
        }
        .login-btn{
            min-width: 200px;
        }
    </style>
</head> 
<body>

	@yield('content')
	
    <script src="{{ URL::asset('assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script> 
    <script src="{{ URL::asset('assets/vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script> 
    <script src="{{ URL::asset('assets/js/app.min.js') }}"></script> 
</body> 
</html>