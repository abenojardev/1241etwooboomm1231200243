@foreach($data as $x)
	<div class="modal fade" id="update_{{ $x->id }}" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
	    <div class="modal-dialog" role="document" ng-app="app" ng-controller="category">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="exampleModalLabel">Update</h5><button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	            </div>
	            <form action="{{ URL::route('app.products.update',$x->id) }}" method="post" enctype="multipart/form-data">
	                @csrf
	                <div class="modal-body"> 
	                    <div ng-if="form">
	                        <div class="form-group mb-4">
	                            <label class="active">Current Category</label>
	                            <input class="form-control" type="text" disabled value="{{ App\Models\ProductCategories::find($x->category_id)->title }}">
	                        </div>  
	                        <div class="form-group mb-4">
	                            <label class="active">Product Name</label>
	                            <input class="form-control" type="text" name="name" required value="{{ $x->name }}">
	                        </div>  
	                        <div class="form-group mb-4">
	                            <label class="active">Part Number</label>
	                            <input class="form-control" type="text" name="partnumber" required value="{{ $x->part_number }}">
	                        </div>  
	                        <div class="form-group mb-4">
	                            <label class="active">SRP</label>
	                            <input class="form-control" type="text" name="srp" required value="{{ $x->srp }}">
	                        </div>  
	                        <div class="form-group mb-4">
	                            <label class="active">Description</label>
	                            <textarea class="form-control" type="text" name="description" required rows="5">
	                            	{{ $x->description }}
	                            </textarea>
	                        </div>  
	                        <div class="form-group mb-4">
	                            <label class="active">Availability</label>
	                            <select class="form-control" type="text" name="availability" required>
	                                <option @if($x->availability == '1') selected @endif value="1">Available</option>
	                                <option @if($x->availability == '0') selected @endif value="0">Not-Available</option>
	                            </select>
	                        </div>  
	                        <div class="form-group mb-4">
	                            <label class="active">Photos</label>
	                            <input class="form-control" type="file" name="photo">
	                        </div>  
	                    </div>
	                </div>
	                <div class="modal-footer">
	                    <button class="btn" type="button" data-dismiss="modal">Close</button>
	                    <button class="btn btn-primary" ng-if="form">Save</button>
	                </div>
	            </form>
	        </div>
	    </div>
	</div>
@endforeach 

















