<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create</h5><button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <form action="{{ URL::route('app.products.categories.create') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">  
                    <div class="form-group mb-4">
                        <label class="active">Category Title</label>
                        <input class="form-control" type="text" name="title" required>
                        <input type="hidden" name="parent_id" value="{{ is_object($parent) ? $parent->id : null }}">
                    </div> 
                    <div class="form-group mb-4">
                        <label class="active">Photo</label>
                        <input class="form-control" type="file" name="photo" required>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>