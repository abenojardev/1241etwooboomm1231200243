@php $active = 'products' @endphp
@php $subactive = 'filters' @endphp
@extends('layouts.main.index')
@section('title','Filters')
@section('content')
    <div class="page-content fade-in-up"> 
        <div class="page-heading">
            <div class="page-breadcrumb">
                <h1 class="page-title">Product Filters
                    <small>
                        < for 
                        <a href="{{ URL::route('app.products.categories',$category->id) }}"> 
                            {{ $category->title}}
                        </a>
                    </small>
                </h1>
            </div> 
            <div>
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#create"> 
                    <span class="ft-plus"></span> Create
                </button>
            </div> 
        </div>
        <div>
            <div class="card">
                <div class="card-body">
                    <h5 class="box-title"></h5>
                    <div class="table-responsive">
                        <table class="table table-bordered w-100" id="dt-base">
                            <thead class="thead-light">
                                <tr> 
                                    <th>Filter</th> 
                                    <th>Items</th> 
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>  
                                @foreach($data as $x)
                                    <tr> 
                                        <td>
                                            <a href="{{ URL::route('app.products.filters.items',$x->id) }}">
                                                {{ $x->title }}
                                            </a>
                                        </td> 
                                        <td>{{ $x->items }}</td>
                                        <td>{{ $x->created_at }}</td>
                                        <td>{{ $x->updated_at }}</td>
                                        <td>
                                            Active
                                        </td>
                                        <td>
                                            <div class="mb-4"> 
                                                <div class="btn-group" role="group">
                                                    <button class="btn btn-light dropdown-toggle" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                        <a class="dropdown-item" data-toggle="modal" data-target="#update_{{ $x->id }}">Update</a>
                                                        <a class="dropdown-item" href="{{ URL::route('app.products.filters.delete',$x->id) }}">Delete</a>
                                                    </div>
                                                </div> 
                                            </div>
                                        </td> 
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.filters.modals.create.index',[ 'category_id' => $category->id ]) 
    @include('pages.filters.modals.update.index',[ 'data' => $data ]) 
@endsection
@section('extraCss')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/vendors/DataTables/datatables.min.css') }}">
@endsection
@section('extraJs')
    <script type="text/javascript" src="{{ URL::asset('assets/vendors/DataTables/datatables.min.js') }}"></script> 
    <script src="{{ URL::asset('assets/js/dt.js') }}"></script>   
@endsection