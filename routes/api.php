<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1','namespace' => 'API'],function(){
	Route::group(['prefix' => 'products','namespace' => 'Product'],function(){ 
		Route::group(['prefix' => 'categories','namespace' => 'Category'], function(){

			Route::get('/',               'ViewController@index'     )->name('api.products.categories');

		});
	});
});
