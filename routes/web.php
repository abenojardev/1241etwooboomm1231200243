<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'backend'],function(){

	Route::group(['middleware' => 'backend.noauth'],function(){

		Route::group(['namespace' => 'Auth\Backend'],function(){

			Route::get('/',                   'LoginController@index'    )->name('app.login'); 
			Route::post('/verify',            'VerifyController@index'   )->name('app.verify'); 

		});
	});

	Route::group(['middleware' => 'backend.auth'],function(){

		Route::group(['prefix' => 'logout','namespace' => 'Auth\Backend'],function(){
			Route::get('/',                   'LogoutController@index'   )->name('app.logout');
		});

		Route::group(['prefix' => 'products','namespace' => 'Product'],function(){

			Route::get('/',                   'ViewController@index'     )->name('app.products');
			Route::get('/view/{id}',          'ViewController@view'      )->name('app.products.view');
			Route::post('/create',            'CreateController@index'   )->name('app.products.create'); 
			Route::post('/update/{id}',       'UpdateController@index'   )->name('app.products.update'); 
			Route::post('/delete/{id}',       'DeleteController@index'   )->name('app.products.delete'); 
			Route::post('/update/specs/{id}', 'UpdateController@specs'   )->name('app.products.specs.update'); 
			Route::post('/update/filter/{id}','UpdateController@filter'  )->name('app.products.filter.update'); 
 
			Route::group(['prefix' => 'categories','namespace' => 'Categories'], function(){

				Route::get('/{id?}',          'ViewController@index'     )->name('app.products.categories');
				Route::post('/create',        'CreateController@index'   )->name('app.products.categories.create');
				Route::post('/update/{id}',   'UpdateController@index'   )->name('app.products.categories.update');
				Route::get('/delete/{id}',    'DeleteController@index'   )->name('app.products.categories.delete');

			});

			Route::group(['prefix' => 'filters','namespace' => 'Filters'], function(){

				Route::get('/{id?}',          'ViewController@index'     )->name('app.products.filters'); 
				Route::post('/create',        'CreateController@index'   )->name('app.products.filters.create'); 
				Route::post('/update/{id}',   'UpdateController@index'   )->name('app.products.filters.update'); 
				Route::get('/delete/{id}',    'DeleteController@index'   )->name('app.products.filters.delete'); 

			});

			Route::group(['prefix' => 'filters/items','namespace' => 'FiltersItems'], function(){

				Route::get('/{id?}',          'ViewController@index'     )->name('app.products.filters.items'); 
				Route::post('/create',        'CreateController@index'   )->name('app.products.filters.items.create'); 
				Route::post('/update/{id}',   'UpdateController@index'   )->name('app.products.filters.items.update');
				Route::get('/delete/{id}',    'DeleteController@index'   )->name('app.products.filters.items.delete');  

			});
  
		});

	});

});
 
// Resources routes
Route::group(['prefix' => 'photos','namespace' => 'Resources'],function(){
	Route::get('/',                 'ImageController@index'   )->name('app.resources');
});