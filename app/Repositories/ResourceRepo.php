<?php 

namespace App\Repositories;
  
use Illuminate\Database\Eloquent\Model;  

class ResourceRepo
{
	
	protected $model;  

	function __construct(Model $model)
	{
		$this->model = $model; 
	}

	public function getById($id){

		$resource = $this->model->find($id);

		return $resource ? $resource : 0;
	}

	public function getAll($parameters = null,$optionals = null,$status = 1,$paginate = false,$sql = false)
	{ 
		$q = $this->model;

		if (!is_null($parameters)) {
			$q = $q->where(function($query) use($parameters){
				foreach ($parameters as $key => $row) {
					$query->where($key,'=',$row);
				}
			});
		}

		if (!is_null($optionals)) {
			$q = $q->where(function($query) use($optionals){
				for ($i=0; $i < count($optionals); $i++) { 
					$val = ($optionals[$i]['operand'] == 'LIKE') ? '%'.$optionals[$i]['val'].'%' : $optionals[$i]['val'];
					$query->orWhere(
						$optionals[$i]['col'],
						$optionals[$i]['operand'],
						$val
					);
				}
			});
		}

		$status = is_null($status) ? 1 : $status;

		$q = $q->whereStatus($status);


 		if ($sql) { 
 			return $q->toSql();
 		}

 		return ($paginate != false) ? $q->paginate($paginate) : $q->get();  


	} 
 
	public function getCount($parameters = null,$optionals = null,$status = 1,$paginate = false,$sql = false)
	{
		$resource = $this->getAll($parameters,$optionals,$status,$paginate,$sql);

		return $resource == false ? 0 : $resource->count(); 
	} 

	public function create(array $attributes)
	{ 	
        try{  
			if($this->model->create($attributes)){
				return 1;
			}  
			return 0; 
        } catch (\Illuminate\Database\QueryException $e){ 
        	return 0;
        } 
	} 
	
	public function update($id,array $attributes)
	{
		$resource = $this->model->find($id); 
		
		if (!$resource) {
			return 0;
		}   

        try{  
			if($resource->update($attributes)){
				return 1;
			}  
			return 0; 
        } catch (\Illuminate\Database\QueryException $e){ 
        	return 0;
        } 
	}

	public function delete($id)
	{
		return $this->update($id,[ 'status' => 0 ]);
	}
}