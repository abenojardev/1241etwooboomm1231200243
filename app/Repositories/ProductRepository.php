<?php 

namespace App\Repositories;
 
use Illuminate\Database\Eloquent\Model; 
use App\Contracts\ProductInterface; 
use App\Repositories\ResourceRepo;
use Auth;

class ProductRepository extends ResourceRepo implements ProductInterface
{
	
	protected $model; 

	function __construct(Model $model)
	{
		$this->model = $model; 
	}  

	public function getMainProducts()
	{
		return $this->getAll([
			'type' => 'main',
			'user_id' => Auth::user()->id
		]);
	}
}