<?php 

namespace App\Repositories;
  
use Illuminate\Database\Eloquent\Model; 
use App\Contracts\UserInterface; 
use App\Repositories\ResourceRepo;

class UserRepository extends ResourceRepo implements UserInterface
{
	
	protected $model; 

	function __construct(Model $model)
	{
		$this->model = $model; 
	}   
}