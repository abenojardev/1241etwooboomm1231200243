<?php 

namespace App\Repositories;
  
use Illuminate\Database\Eloquent\Model; 
use App\Contracts\CategoryInterface; 
use App\Repositories\ResourceRepo;

class CategoryRepository extends ResourceRepo implements CategoryInterface
{
	
	protected $model; 

	function __construct(Model $model)
	{
		$this->model = $model; 
	} 

	public function getCategoriesForTables($id)
	{
		$data = $this->getAll(['parent_id'=> $id]);

		foreach ($data as $key => $value) {
			$value->sub = $this->getCount(['parent_id'=>$value->id]);
		}

		return $data;
	}

	public function getParentCategory($id)
	{

		return is_null($id) ? null : $this->getById($id);
	}
}