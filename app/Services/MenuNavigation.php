<?php 

namespace App\Services; 

/**
 * Returns object of menu for users navigation
 *
 * @class MenuNavigation
 */ 
 
class MenuNavigation {  

    /**
     * constant type types
     *
     * @var type
     */  
    const A = 'admin';
    const B = 'supplier';
    const C = 'manufacturer';

    public static function setup()
    {    
        $type = new MenuNavigation;

        return [
            [
                'type' => 'single',
                'active' => 'dashboard',
                'icon' => 'ft-home',
                'name' => 'Dashboard',
                'url' => 'app.login',
                'access' => [
                    $type::A,
                    $type::B,
                    $type::C
                ]
            ],
            [
                'type' => 'multi',
                'active' => 'products',
                'icon' => 'ft-shopping-cart',
                'name' => 'Products',
                'url' => '',
                'access' => [
                    $type::A,$type::C
                ],
                'submenu' => [
                    [
                        'active' => 'categories',
                        'name' => 'Categories',
                        'url' => 'app.products.categories'
                    ],
                    [
                        'active' => 'products',
                        'name' => 'Products',
                        'url' => 'app.products'
                    ]
                ]
            ],  
            [
                'type' => 'multi',
                'active' => 'accounts',
                'icon' => 'ft-users',
                'name' => 'Accounts',
                'url' => '',
                'access' => [
                    $type::A
                ],
                'submenu' => [
                    [
                        'active' => 'suppliers',
                        'name' => 'Suppliers',
                        'url' => 'app.products.categories'
                    ],
                    [
                        'active' => 'manufacturer',
                        'name' => 'Manufacturer',
                        'url' => 'app.products'
                    ],
                    [
                        'active' => 'users',
                        'name' => 'Users',
                        'url' => 'app.products'
                    ]
                ]
            ],  
            [
                'type' => 'single',
                'active' => 'logout',
                'icon' => 'ft-settings',
                'name' => 'Logout',
                'url' => 'app.logout',
                'access' => [
                    $type::A,
                    $type::B,
                    $type::C
                ],
            ]
        ]; 
    }
}