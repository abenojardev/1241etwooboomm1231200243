<?php 

namespace App\Contracts; 

interface CategoryInterface {
	
	public function getCategoriesForTables($id);
	  
	public function getParentCategory($id);
}