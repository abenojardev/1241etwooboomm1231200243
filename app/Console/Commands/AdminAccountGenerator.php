<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\UserRepository;
use App\Models\User;

class AdminAccountGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate admin account, default is admin/1234';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    { 
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(User $user)
    {
        $user = new UserRepository($user);

        $data = [
            'name' => getenv('APP_NAME'),
            'email' => 'administrator',
            'password' => bcrypt('12341234'),
            'type' => 'admin',
            'status' => 1
        ];

        $data2 = [
            'name' => 'supplier',
            'email' => 'supplier',
            'password' => bcrypt('12341234'),
            'type' => 'supplier',
            'status' => 1
        ];

        $data3 = [
            'name' => 'manufacturer',
            'email' => 'manufacturer',
            'password' => bcrypt('12341234'),
            'type' => 'manufacturer',
            'status' => 1
        ];

        $save = $user->create($data);

        if ($save) {
            $this->info('Admin account has been successfully created !');
        } else { 
            return $this->error($save);
        }
    }
}
