<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
	protected $table = 'products_categories';

	protected $fillable = ['title','parent_id','photo','status'];
}
