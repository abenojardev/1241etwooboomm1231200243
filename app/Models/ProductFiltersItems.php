<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFiltersItems extends Model
{
	protected $table = 'product_filters_items';

	protected $fillable = ['product_filters_id','type','status'];
}
