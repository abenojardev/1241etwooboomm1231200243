<?php

namespace App\Http\Controllers\API\Product\Category;

use App\Http\Controllers\Controller; 
use App\Models\ProductCategories;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request; 

class ViewController extends Controller
{  
    protected $categories,$request;

    public function __construct(ProductCategories $categories,Request $request)
    {
        $this->request = $request;
        $this->categories = new CategoryRepository($categories);
    }

    public function index()
    {     
        $id = $this->request->id;
        $data = $this->categories->getCategoriesForTables($id); 

        return response()->json([
            'status' => 200,
            'data' => $data
        ],200,[],JSON_PRETTY_PRINT);
    }
}
