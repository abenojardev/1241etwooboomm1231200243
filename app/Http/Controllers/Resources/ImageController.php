<?php

namespace App\Http\Controllers\Resources;

use Storage; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 

class ImageController extends Controller
{ 
    protected $request;

    public function __construct(Request $request)
    { 
        $this->request = $request;
    }

    public function index()
    {  
        $img = $this->request->src; 

        if(!Storage::disk('local')->has($img)){
            abort(400);
        } 
        
        $path = storage_path('app/'.$img); 
        $img = file_get_contents($path);

        return response($img)->header('Content-type','image/png');
    }
}
