<?php

namespace App\Http\Controllers\Product\Filters;

use App\Http\Controllers\Controller; 
use App\Models\ProductFilters;
use App\Repositories\FilterRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;

class UpdateController extends Controller
{  
    protected $filters,$request;

    public function __construct(ProductFilters $filters,Request $request)
    {
        $this->request = $request;
        $this->filters = new FilterRepository($filters);
    }

    public function index($id)
    { 
        $attributes = [ 
            'title' => $this->request->title 
        ];

        $save = $this->filters->update($id,$attributes);
        
        return $save ? back() : abort(500);
    } 
}
