<?php

namespace App\Http\Controllers\Product\Filters;

use App\Http\Controllers\Controller; 
use App\Repositories\CategoryRepository;
use App\Repositories\FilterRepository;
use App\Models\ProductCategories;
use App\Models\ProductFilters;
use Illuminate\Http\Request;
use Storage;

class ViewController extends Controller
{  
    protected $filters,$categories,$request;

    public function __construct(ProductFilters $filters,ProductCategories $categories,Request $request)
    {
        $this->request = $request;
        $this->filters = new FilterRepository($filters);
        $this->categories = new CategoryRepository($categories);
    }

    public function index($id = null)
    {     
        $data = $this->filters->getFiltersForTables($id); 
        $category = $this->categories->getById($id);
        
        return view('pages.filters.table.index')
            ->with([
                'data' => $data,
                'category' => $category
            ]);
    }
}
