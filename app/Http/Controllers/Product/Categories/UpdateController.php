<?php

namespace App\Http\Controllers\Product\Categories;

use App\Http\Controllers\Controller; 
use App\Models\ProductCategories;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;

class UpdateController extends Controller
{  
    protected $categories,$request;

    public function __construct(ProductCategories $categories,Request $request)
    {
        $this->request = $request;
        $this->categories = new CategoryRepository($categories);
    }

    public function index($id)
    { 
        $attributes = [ 
            'title' => $this->request->title 
        ];

        if($this->request->has('photo') && isset($this->request->photo)) {
            $attributes['photo'] = $this->upload();
        }

        $save = $this->categories->update($id,$attributes);
        
        return $save ? back() : abort(500);
    }

    public function upload()
    {
        $disk = new DiskStorage($this->request->photo,'categories');
        return $disk->generate();
    }
}
