<?php

namespace App\Http\Controllers\Product\FiltersItems;

use App\Http\Controllers\Controller; 
use App\Repositories\FilterItemsRepository;
use App\Repositories\FilterRepository;
use App\Models\ProductFiltersItems;
use App\Models\ProductFilters;
use Illuminate\Http\Request;
use Storage;

class ViewController extends Controller
{  
    protected $filters,$items,$request;

    public function __construct(ProductFilters $filters,ProductFiltersItems $items,Request $request)
    {
        $this->request = $request;
        $this->filters = new FilterRepository($filters);
        $this->items = new FilterItemsRepository($items);
    }

    public function index($id = null)
    {     
        $data = $this->items->getFiltersItemsForTables($id);
        $filter = $this->filters->getById($id); 

        return view('pages.filtersitems.table.index')
            ->with([
                'data' => $data,
                'filter' => $filter 
            ]);
    }
}
