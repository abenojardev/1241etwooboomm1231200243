<?php

namespace App\Http\Controllers\Product\FiltersItems;

use App\Http\Controllers\Controller; 
use App\Models\ProductFiltersItems;
use App\Repositories\FilterItemsRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;

class CreateController extends Controller
{  
    protected $items,$request;

    public function __construct(ProductFiltersItems $items,Request $request)
    {
        $this->request = $request;
        $this->items = new FilterItemsRepository($items);
    }

    public function index()
    {
 
        $attributes = [
            'product_filters_id' => $this->request->filter_id,
            'type' => $this->request->title, 
            'status' => 1
        ];

        $save = $this->items->create($attributes);
        
        return $save ? back() : abort(500);
    } 
}
