<?php

namespace App\Http\Controllers\Product\FiltersItems;

use App\Http\Controllers\Controller; 
use App\Models\ProductFiltersItems;
use App\Repositories\FilterItemsRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;

class DeleteController extends Controller
{  
    protected $items,$request;

    public function __construct(ProductFiltersItems $items,Request $request)
    {
        $this->request = $request;
        $this->items = new FilterItemsRepository($items);
    }

    public function index($id)
    { 

        $save = $this->items->delete($id);
        
        return $save ? back() : abort(500);
    } 
}
