<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller; 
use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Models\ProductCategories;
use App\Models\ProductFilters;
use App\Repositories\CategoryRepository;
use App\Repositories\FilterRepository;
use Illuminate\Http\Request;
use Storage;

class ViewController extends Controller
{  
    protected $categories,$request,$filters,$products;

    public function __construct(Product $products,ProductFilters $filters,ProductCategories $categories,Request $request)
    {
        $this->request = $request;
        $this->products = new ProductRepository($products);
        $this->categories = new CategoryRepository($categories);
        $this->filters = new FilterRepository($filters);
    }
  
    public function index()
    {
        $data = $this->products->getMainProducts();
        $categories = $this->categories->getCategoriesForTables(null);

        return view('pages.products.table.index')
            ->with([
                'data' => $data,
                'category' => $categories
            ]);
    }
    
    public function view($id)
    {
        $data = $this->products->getById($id);
        $filters = $this->filters->getFiltersForAll($data->category_id);

        if(is_null($data->specs)) {
            $data->specs = [];
        } else {
            $data->specs = json_decode($data->specs);
        } 

        if(is_null($data->filters)) {
            $data->filters = [];
        } else {
            $data->filters = json_decode($data->filters,true);
        } 

        return view('pages.products.details.index')
            ->with([
                'data' => $data,
                'filters' => $filters
            ]);
    }

}
