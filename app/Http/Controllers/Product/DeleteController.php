<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller; 
use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;
use Auth;

class DeleteController extends Controller
{  
    protected $product,$request;

    public function __construct(Product $product,Request $request)
    {
        $this->request = $request;
        $this->product = new ProductRepository($product);
    }

    public function index($id)
    {   
        $save = $this->product->delete($id);
        
        return $save ? back() : abort(500);
    }
}
