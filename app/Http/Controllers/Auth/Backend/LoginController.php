<?php

namespace App\Http\Controllers\Auth\Backend;

use App\Http\Controllers\Controller;  

class LoginController extends Controller
{   

    public function __construct()
    {
        //
    }

    public function index()
    {
        return view('pages.authentication.login.backend.index');     
    }
}
