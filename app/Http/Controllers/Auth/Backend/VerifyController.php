<?php

namespace App\Http\Controllers\Auth\Backend;

use App\Http\Controllers\Controller;  
use Illuminate\Http\Request; 
use App\Models\User;
use Auth,Redirect;

class VerifyController extends Controller
{   

	protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
    	$credentials = [
    		'email' => $this->request->email,
    		'password' => $this->request->password,
    		'status' => 1
    	];

    	if(Auth::attempt($credentials)){
    		return Redirect::route('app.products.categories');
    	}

    	return back()->withError('Invalid login credentials !');
    }
}
