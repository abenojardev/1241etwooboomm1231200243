<?php

namespace App\Http\Controllers\Auth\Backend;

use App\Http\Controllers\Controller;  
use Illuminate\Http\Request; 
use App\Models\User;
use Auth,Redirect;

class LogoutController extends Controller
{   

	protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        Auth::logout();

        return Redirect::route('app.login');
    }
}
